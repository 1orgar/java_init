import java.util.Scanner;

public class Task02N031 {
    public static void main(String args[]) {
        int n;
        System.out.print("Enter the number between 100 and 999: ");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        int x = (n % 10) * 10 + n / 100 * 100 + (n % 100) / 10;
        System.out.println("X=" + x);
    }
}
