import java.text.DecimalFormat;

public class Task05N038 {
    public static void main(String[] args) {
        double path_to_work = 1.0;
        int iterations = 100;
        double path_walked = 0.0, distance_from_home = 0.0;
        DecimalFormat df = new DecimalFormat("#.### km");

        for(int i = 1; i <= iterations; i++) {
            path_walked += path_to_work / i;
            distance_from_home += (i % 2 == 1) ? path_to_work / i : -(path_to_work / i);
        }
        System.out.println("Total path that took a man: " + df.format(path_walked));
        System.out.println("Distance from a man's home: " + df.format(distance_from_home));
    }
}
