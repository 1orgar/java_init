import java.util.Scanner;

public class Task06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        while(!game.id_over()) {
            game.play();
            System.out.println(game.score());
        }
        System.out.println(game.result());
    }
}


class Game {
    private String team1, team2;
    private int score_t1, score_t2;
    private boolean game_over;
    private Scanner sc;
    Game() {
        score_t1 = score_t2 = 0;
        game_over = false;
        sc = new Scanner(System.in);

        System.out.print("Enter team #1: ");
        team1 = sc.next();
        System.out.print("Enter team #2: ");
        team2 = sc.next();
    }
    public void play() {
        System.out.print("Enter team to score (1 or 2 or 0 to finish game): ");
        int team = sc.nextInt();
        if(team == 0) {
            game_over = true;
        } else if (team == 1 || team == 2) {
            System.out.print("Enter score (1 or 2 or 3): ");
            int score = sc.nextInt();
            if(score < 1 || score > 3)
                throw new RuntimeException("Something goes wrong");
            if(team == 1)
                score_t1 += score;
            else
                score_t2 += score;
        }
        else
            throw new RuntimeException("Something goes wrong");
    }
    public String score() {
        return "Team #1 (" + team1 + ") score " + score_t1 + ", team #2 (" + team2 + ") score " + score_t2;
    }
    public String result() {
        if(score_t1 > score_t2)
            return "Results:\nTeam #1 (" + team1 + ") is winner, score " + score_t1 + ", team #2 (" + team2 + ") is looser, score " + score_t2;
        else if(score_t2 > score_t1)
            return "Results:\nTeam #2 (" + team2 + ") is winner, score " + score_t2 + ", team #1 (" + team1 + ") is looser, score " + score_t1;
        else
            return "Results:\nDraw! score " + score_t1;
    }
    public boolean id_over() {
        return game_over;
    }
}