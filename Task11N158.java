public class Task11N158 {
    public static void main(String[] args) {
        int [] array = {10, 13, 17, 10, 19, 10, 17, 19, 13};

        for(int i = 0; i < array.length; i++) {
            int x = array[i];
            for(int j = i + 1; j < array.length; j++) {
                if(x == array[j]) {
                    for(int n:array) {
                        System.out.print(n + " ");
                    }
                    System.out.println("");
                    System.arraycopy(array, j + 1, array, j, array.length - j - 1);
                    array[array.length - 1] = 0;
                }
            }
        }
    }
}
