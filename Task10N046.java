public class Task10N046 {
    public static void main(String[] args) {
        int first_member = 10, den = 5, n = 10;
        System.out.println(n + " member of sequence = " + memb(n, first_member, den));
        System.out.println("Sum of first " + n + " members of sequence = " + sum(n, first_member, den));
    }
    static int memb(int n, int first, int den) {
        if(n == 1) return first;
        else return den * memb(n - 1, first, den);
    }
    static int sum(int n, int first, int den) {
        if(n == 1) return first;
        else return memb(n, first, den) + sum(n - 1, first, den);
    }
}
