public class Task10N043 {
    public static void main(String[] args) {
        int number = 123456;
        System.out.println("Number " + number + " the sum of its digits " + digitsSum(number));
        System.out.println("Number " + number + " has " + digitsNum(number) + " digits");
    }
    static int digitsSum(int n) {
        if(n / 10 == 0) return n % 10;
        else return n % 10 + digitsSum(n / 10);
    }
    static int digitsNum(int n) {
        if(n / 10 == 0) return 1;
        else return digitsNum(n / 10) + 1;
    }
}
