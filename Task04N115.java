import java.util.Scanner;
import java.util.Random;

public class Task04N115 {
    public static void main(String[] args) {
        int year;
        Scanner sc = new Scanner(System.in);

        while(true) {
            System.out.print("Enter the year >=1984: ");
            year = sc.nextInt();
            if(year >= 1984)
                break;
            System.out.println("Incorrect value!");
        }
        System.out.println("Год: " + year + ", " + GetChineseCalYear(year));

        Random rnd = new Random();
        year = rnd.nextInt(-1 >>> 1) + 1; // Year == any random natural number
        System.out.println("Год (случайно): " + year + " - " + GetChineseCalYear(year));
    }

    static String GetChineseCalYear(int year) {
        String [] animals = {"Крыса", "Корова", "Тигр", "Заяц", "Дракон", "Змея",
                "Лошадь", "Овца", "Обезьяна", "Петух", "Собака", "Свинья"};
        String [] colors = {"зеленый", "красный", "желтый", "белый", "черный"};
        year -= 1984;
        int animal = year >= 0 ? year % 12 : 11 + ((year + 1) % 12);
        int color = year >= 0 ? (year / 2) % 5 : 4 + ((year + 1) / 2) % 5;
        return  animals[animal] + ", " + colors[color];
    }

}