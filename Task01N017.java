import java.lang.Math;

public class Task01N017 {
    public static void main(String args[]) {
        double  a = 2.0, b = 4.0, c = 7.0;
        double x = 10.0;
        double result_o, result_p, result_r, result_s;
        result_o = Math.sqrt(1 - Math.pow(Math.sin(x), 2));
        result_p = 1 / (Math.sqrt(a * x * x + b * x + c));
        result_r = (Math.sqrt(x + 1) + Math.sqrt(x - 1)) / (2 * Math.sqrt(x));
        result_s = Math.abs(x) + Math.abs(x + 1);
        System.out.println("O: " + result_o + "P: " + result_p + "R: " + result_r + "S: " + result_s);
    }
}
