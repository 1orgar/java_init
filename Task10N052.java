public class Task10N052 {
    public static void main(String[] args) {
        int naturalNumber = 1234567890;
        System.out.println("Natural number: " + naturalNumber);
        printNumber(naturalNumber);
    }
    static void printNumber(int n) {
        System.out.println(n % 10);
        if(n / 10 != 0)
            printNumber(n / 10);
    }
}
