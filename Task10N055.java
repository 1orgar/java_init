public class Task10N055 {
    public static void main(String[] args) {
        int number = 123456789, base = 16;
        convertNum(number, base);
    }
    static void convertNum(int num, int base) {
        char [] arrChars = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        if(num / base != 0)
            convertNum(num / base, base);
        System.out.print(arrChars[num % base]);
    }
}
