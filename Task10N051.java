public class Task10N051 {
    public static void main(String[] args) {
        int n = 5;
        proc1(n);
        System.out.println();
        proc2(n);
        System.out.println();
        proc3(n);
    }
    static void proc1(int n) {
        if(n > 0) {
            System.out.println(n);;
            proc1(n - 1);
        }
    }
    static void proc2(int n) {
        if(n > 0) {
            proc2(n - 1);
            System.out.println(n);;
        }
    }
    static void proc3(int n) {
        if(n > 0) {
            System.out.println(n);;
            proc3(n - 1);
            System.out.println(n);;
        }
    }
}
