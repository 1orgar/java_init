public class Task12N023 {
    public static void main(String[] args) {
        int n = 9;
        int [][] array1 = new int[n][n], array2 = new int[n][n], array3 = new int[n][n];

        System.out.println("A");
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                if(i == j || i + j == n - 1)
                    array1[i][j] = 1;
                System.out.print(array1[i][j] + " ");
            }
            System.out.println("");
        }

        System.out.println("B");
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                if(i == j || i + j == n - 1 || i == n / 2 || j == n / 2)
                    array2[i][j] = 1;
                System.out.print(array2[i][j] + " ");
            }
            System.out.println("");
        }

        System.out.println("C");
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                if((j <= n - i - 1 && i <= j) || (j >= n - i - 1 && i >= j))
                    array3[i][j] = 1;
                System.out.print(array3[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
