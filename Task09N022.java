import java.util.Scanner;

public class Task09N022 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = " "; // odd length
        while((str.length() % 2) == 1) {
            System.out.print("Enter a word that contains an even number of characters: ");
            str =  sc.next();
        }
        String res = str.substring(0, str.length() / 2);
        System.out.println("The resulting word was cut to the 1st half: " + res);
    }
}