import java.util.Scanner;

public class Task09N107 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the word: ");
        String str = sc.next();
        StringBuilder sb = new StringBuilder(str);
        if (str.indexOf('a') != -1 && str.lastIndexOf('o') != -1) {
            sb.setCharAt(str.indexOf('a'), 'o');
            sb.setCharAt(str.lastIndexOf('o'), 'a');
        }
        System.out.println("Result: " + sb);
    }
}