import java.util.Scanner;

public class Task09N015 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the word: ");
        String str = sc.next();
        System.out.print("Enter a symbol position in that word: ");
        int pos = sc.nextInt();
        char ch = str.charAt(pos - 1);
        System.out.println(pos + " symbol is \"" + ch + "\"");
    }
}
