public class Test04N036 {
    public static void main(String args[]) {
        TrafficLight tl = new TrafficLight();
        double t = 3.0;
        System.out.println(tl.get_light_state(t));
        t = 5.0;
        System.out.println(tl.get_light_state(t));
    }
}

enum Light {
    RED,
    GREEN,
}
class TrafficLight {
    private Light state;
    TrafficLight() {
        state = Light.GREEN;
    }
    public Light get_light_state() {
        return state;
    }
    public Light get_light_state(double timestamp) {
        int iters = (int)timestamp / 5;
        if(timestamp - iters * 5 < 3.0)
            state = Light.GREEN;
        else
            state = Light.RED;
        return state;
    }
}