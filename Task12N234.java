public class Task12N234 {
    public static void main(String[] args) {
        int n = 7, m = 9, k = 2, s = 4;
        int [][] src_arr = new int [n][m];
        int [][] dest_arr = new int [n - 1][m - 1];

        // fill
        int value = 0;
        for(int i = 0; i < src_arr.length; i++)
            for(int j = 0; j < src_arr[i].length; j++)
                src_arr[i][j] = ++value;
        printArray(src_arr);

        // delete
        for(int i = 0; i < src_arr.length; i++)
            for(int j = 0; j < src_arr[i].length; j++)
                dest_arr[i >= k ? i - 1: i][j >= s ? j - 1: j] = src_arr[i][j];
        printArray(dest_arr);


    }
    static void printArray(int [][] array) {
        for(int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++)
                System.out.print(array[i][j] + "\t");
            System.out.println();
        }
        System.out.println();
    }
}
