import java.util.Random;

public class Task05N064 {
    public static void main(String[] args) {
        int [][] districts = new int[12][2];
        Random rnd = new Random();

        for(int [] i: districts) { // fill data
            i[0] = rnd.nextInt(50) * 1000; // district population *1000
            i[1] = rnd.nextInt(1000) + 1; // district area cannot be 0
        }
        for(int i = 0; i < districts.length; i++) { // display data
            System.out.println("District #" + (i + 1) + ", population: " + districts[i][0] + ", area: " + districts[i][1] + " square km");
        }
        System.out.println("Average population density: " + calc_avg_pop_density(districts) + " people per square km");

    }
    static int calc_avg_pop_density(int [][] districts) {
        int total_population = 0, total_area = 0;
        for(int [] i: districts) {
            total_population += i[0];
            total_area += i[1];
        }
        return (int)((double)total_population / (double)total_area);
    }
}
