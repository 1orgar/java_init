public class Task10N049 {
    public static void main(String[] args) {
        int [] arrInt = {10, 2, 7, 9, 5, 12, 54, 3, 14, 6};
        System.out.println("Max array element index is " + getMaxIndex(arrInt, 0));
    }
    static int getMaxIndex(int [] array, int index) {
        if(index == array.length) return 0;
        int k = getMaxIndex(array, index + 1);
        return array[index] < array[k] ? k : index;
    }
}
