import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task09N185 {
    public static void main(String[] args) {
        CheckBrackets cb = new CheckBrackets("(a+(d)+(c)+f)+(m+(g+j))+(f*8))");
        if(cb.state == States.OK) {
            System.out.println("Yes");
        }
        else {
            System.out.println("No");
            if(cb.state == States.CLOSE_EXCEEDS || cb.state == States.CLOSE_BRACKET_FIRST) {
                System.out.println("Extra right (closing) brackets, 1st position of this bracket: " + (cb.first_close + 1));
            }
            else if(cb.state == States.OPEN_EXCEEDS || cb.state == States.OPEN_BRACKET_LAST) {
                System.out.println("Extra left (opening) brackets, total excess brackets of this type: " + cb.excess_open);
            }
        }
    }
}

enum States{
    OK,
    NESTED_WRONG,
    CLOSE_BRACKET_FIRST,
    OPEN_BRACKET_LAST,
    OPEN_EXCEEDS,
    CLOSE_EXCEEDS,
}

class CheckBrackets {
    public States state;
    public int first_open, first_close, last_open, last_close;
    public int excess_open, excess_close;

    CheckBrackets(String str) {
        first_open = str.indexOf('(');
        first_close = str.indexOf(')');
        last_open = str.lastIndexOf('(');
        last_close = str.lastIndexOf(')');

        for(char x: str.toCharArray()) {
            if(x == '(') {
                excess_open++;
                excess_close--;
            }
            if(x == ')') {
                excess_open--;
                excess_close++;
            }
        }

        if(first_close != -1 && first_close < first_open) {
            state = States.CLOSE_BRACKET_FIRST;
        }
        else if(last_open != -1 && last_open > last_close) {
            state = States.OPEN_BRACKET_LAST;
        }
        else if(excess_close > excess_open) state = States.CLOSE_EXCEEDS;
        else if(excess_close < excess_open) state = States.OPEN_EXCEEDS;
        else if(first_open != -1 && last_close != -1 && check_nested(str) == 0)
            state = States.OK;
        else state = States.NESTED_WRONG;
    }
    private int check_nested(String str) {
        int o = str.indexOf('('), c = str.lastIndexOf(')');
        int bracket = 0;
        Pattern p = Pattern.compile("\\(([^()]*|\\([^()]*\\))*\\)");
        Matcher m = p.matcher(str);
        while (m.find()) {
            String str_c = str.substring(m.start() + 1, m.end() - 1);
            check_nested(str_c);
            for(char x: str_c.toCharArray()) {
                if (x == '(') bracket++;
                if (x == ')') bracket--;
            }
            return bracket;
        }
        return 0;
    }
}