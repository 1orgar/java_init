import java.text.DecimalFormat;
import java.util.Scanner;
import java.text.DecimalFormat;

public class Task05N010 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("USD/RUB exchange rate: ");
        double rate = sc.nextDouble();
        DecimalFormat df = new DecimalFormat("#.00");

        for(int i = 1; i <= 20; i++) {
            System.out.println("$" + i + " = " + df.format(rate * i) + " RUB");
        }
    }
}
