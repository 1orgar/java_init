public class Task12N028 {
    public static void main(String[] args) {
        int n = 7;
        int [][] arr = new int[n][n];
        int t = 1, b = n - 1, r = n - 1, l = 0;
        int k = 1;
        int direction = 0;
        int i = 0, j = 0;

        while(k != n * n + 1) {
            arr[i][j] = k++;
            switch (direction) {
                case 0:
                    if(i < r) i++;
                    else {
                        direction = 1;
                        r--;
                        j++;
                    }
                    break;
                case 1:
                    if(j < b) j++;
                    else {
                        direction = 2;
                        b--;
                        i--;
                    }
                    break;
                case 2:
                    if(i > l) i--;
                    else {
                        direction = 3;
                        l++;
                        j--;
                    }
                    break;
                case 3:
                    if(j > t) j--;
                    else {
                        direction = 0;
                        t++;
                        i++;

                    }
                    break;
            }

        }
        printArray(arr);
    }
    static void printArray(int [][] array) {
        for(int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++)
                System.out.print(array[j][i] + "\t");
            System.out.println();
        }
        System.out.println();
    }
}
