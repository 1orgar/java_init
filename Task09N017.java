import java.util.Scanner;

public class Task09N017 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the word: ");
        String str =  sc.next();
        if(str.charAt(0) == str.charAt(str.length() - 1)) {
            System.out.println("The first and last characters of this word are equal");
        }
        else {
            System.out.println("The first and last characters of this word are not equal");
        }
    }
}