public class Task04N067 {
    public static void main(String args[]) {
        Year y = new Year(WeekDays.Monday);

        y.set_day(5);
        if (y.is_weekend())
            System.out.println("Weekend");
        else
            System.out.println("Workday");

        y.set_day(7);
        if (y.is_weekend())
            System.out.println("Weekend");
        else
            System.out.println("Workday");
    }
}
enum WeekDays {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
}
class Year {
    private int day, week;
    private WeekDays year_start_weekday, weekday;
    Year(WeekDays year_start_weekday) {
        day = week = 1;
        this.year_start_weekday = weekday = year_start_weekday;
    }
    public void set_day(int day) {
        if(day > 365 || day < 1) throw new RuntimeException("Wrong day");;
        this.day = day; // day 1..365
        int day_s = this.day + year_start_weekday.ordinal(); // shift beside at start of the year
        week = (day_s - 1) / 7 + 1; // current week 1..52
        weekday = WeekDays.values()[(day_s - 1) - (week - 1) * 7]; // weekday 0..6
    }
    public boolean is_weekend() {
        return  weekday == WeekDays.Saturday || weekday == WeekDays.Sunday;
    }
}