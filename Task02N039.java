public class Task02N039 {
    public static void main(String args[]) {
        int h = 23;
        int m = 59;
        int s = 59;
        int h12 = h % 12;
        double h_grad_inc = 360.0 / 12.0 * h12;
        double m_grad_inc = 360.0 / 12.0 / 60.0 * m;
        double s_grad_inc = 360.0 / 12.0 / 60.0 / 60.0 * s;
        double res_degree = h_grad_inc + m_grad_inc + s_grad_inc;
        System.out.println(h + " hours " + m + " minutes " + s + " seconds; " + res_degree + " degree");
    }
}
