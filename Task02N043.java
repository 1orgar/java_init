public class Task02N043 {
    public static void main(String args[]) {
        int a = 10;
        int b = 7;
        int ab = a % b;
        int ba = b % a;
        int res = ab * ba + 1;
        System.out.println("Result: " + res);
    }
}
