public class Task10N041 {
    public static void main(String[] args) {
        long num = 11;
        System.out.println("Factorial of " + num + " is " + factorial(num));
    }
    static long factorial(long num) {
        if(num == 0) return 1;
        else if (num < 0) return 0;
        else return factorial(num - 1) * num;
    }
}
