public class Task02N013 {
    public static void main(String args[]) {
        int num = 175;
        int s1 = num / 100;
        int s2 = (num / 10) % 10;
        int s3 = num % 10;
        int result = s3 * 100 + s2 * 10 + s1;
        System.out.println("Number: " + num + ", result: " + result);
    }
}
