public class Task10N047 {
    public static void main(String[] args) {
        long n = 45;
        System.out.println("Fibonacci number of " + n + " = " + fb(n));
    }
    static long fb(long num) {
        if(num == 0) return 0;
        else if(num == 1) return 1;
        else return fb(num - 1) + fb( num - 2);
    }
}
