public class Task10N044 {
    public static void main(String[] args) {
        int num = 238746813;
        System.out.println("Digital root of " + num + " is " + dgrt(num));
    }
    static int digitsSum(int n) {
        if(n / 10 == 0) return n;
        else return n % 10 + digitsSum(n / 10);
    }
    static int dgrt(int n) {
        if(n / 10 == 0) return n;
        else return dgrt(digitsSum(n));
    }
}