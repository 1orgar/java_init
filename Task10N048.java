public class Task10N048 {
    public static void main(String[] args) {
        int [] arrInt = {10, 2, 7, 9, 5, 12, 54, 3, 14, 6};
        System.out.println("Max array element is " + getMax(arrInt, 0));
    }
    static int getMax(int [] array, int index) {
        if(index == array.length) return 0;
        int k = getMax(array, index + 1);
        return array[index] < k ? k : array[index];
    }
}
