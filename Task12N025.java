public class Task12N025 {
    public static void main(String[] args) {
        int [][] array = new int[12][10];
        for(int i = 0, k = 0; i < 12; i++) {
            for (int j = 0; j < 10; j++) {
                array[i][j] = ++k;
            }
        }
        printArray(array);

        array = new int[12][10];
        for (int j = 0, k = 0; j < 10; j++) {
            for(int i = 0; i < 12; i++) {
                    array[i][j] = ++k;
            }
        }
        printArray(array);

        array = new int[12][10];
        for(int i = 0, k = 0; i < 12; i++) {
            for (int j = array[0].length - 1; j >= 0; j--) {
                array[i][j] = ++k;
            }
        }
        printArray(array);

        array = new int[12][10];
        for (int j = 0, k = 0; j < 10; j++) {
            for(int i = array.length - 1; i >= 0; i--) {
                array[i][j] = ++k;
            }
        }
        printArray(array);

        array = new int[10][12];
        for(int i = 0, k = 0; i < 10; i++) {
            if(i % 2 == 0)
                for (int j = 0; j < 12; j++)
                    array[i][j] = ++k;
            else
                for (int j = array[0].length - 1; j >= 0; j--)
                    array[i][j] = ++k;
        }
        printArray(array);

        array = new int[12][10];
        for(int j = 0, k = 0; j < 10; j++) {
            if(j % 2 == 0)
                for (int i = 0; i < 12; i++)
                    array[i][j] = ++k;
            else
                for (int i = array.length - 1; i >= 0; i--)
                    array[i][j] = ++k;
        }
        printArray(array);

        array = new int[12][10];
        for(int i = array.length - 1, k = 0; i >= 0; i--) {
            for (int j = 0; j < 10; j++)
                array[i][j] = ++k;
        }
        printArray(array);

        array = new int[12][10];
        for (int j = array[0].length - 1, k = 0; j >= 0; j--)
            for(int i = 0; i < 12; i++) {
                array[i][j] = ++k;
        }
        printArray(array);

        array = new int[12][10];
        for(int i = array.length - 1, k = 0; i >= 0; i--) {
            for (int j = array[0].length - 1; j >= 0; j--)
                array[i][j] = ++k;
        }
        printArray(array);

        array = new int[12][10];
        for (int j = array[0].length - 1, k = 0; j >= 0; j--)
            for(int i = array.length - 1; i >= 0; i--) {
                array[i][j] = ++k;
            }
        printArray(array);

        array = new int[12][10];
        for(int i = array.length - 1, k = 0; i >= 0; i--) {
            if(i % 2 == 1)
                for (int j = 0; j < 10; j++)
                    array[i][j] = ++k;
            else
                for (int j = array[0].length - 1; j >= 0; j--)
                    array[i][j] = ++k;
        }
        printArray(array);

        array = new int[12][10];
        for(int i = 0, k = 0; i < 12; i++) {
            if(i % 2 == 1)
                for (int j = 0; j < 10; j++)
                    array[i][j] = ++k;
            else
                for (int j = array[0].length - 1; j >= 0; j--)
                    array[i][j] = ++k;
        }
        printArray(array);

        array = new int[12][10];
        for(int j = array[0].length - 1, k = 0; j >= 0; j--) {
            if(j % 2 == 1)
                for (int i = 0; i < 12; i++)
                    array[i][j] = ++k;
            else
                for (int i = array.length - 1; i >= 0; i--)
                    array[i][j] = ++k;
        }
        printArray(array);

        array = new int[12][10];
        for(int j = 0, k = 0; j < 10; j++) {
            if(j % 2 == 1)
                for (int i = 0; i < 12; i++)
                    array[i][j] = ++k;
            else
                for (int i = array.length - 1; i >= 0; i--)
                    array[i][j] = ++k;
        }
        printArray(array);

        array = new int[12][10];
        for(int i = array.length - 1, k = 0; i >= 0; i--) {
            if(i % 2 == 0)
                for(int j = 0; j < 10; j++)
                    array[i][j] = ++k;
            else
                for (int j = array[0].length - 1; j >= 0; j--)
                    array[i][j] = ++k;
        }
        printArray(array);

        array = new int[12][10];
        for(int j = array[0].length - 1, k = 0; j >= 0; j--) {
            if(j % 2 == 0)
                for(int i = 0; i < 12; i++)
                    array[i][j] = ++k;
            else
                for (int i = array.length - 1; i >= 0; i--)
                    array[i][j] = ++k;
        }
        printArray(array);
    }


    static void printArray(int [][] array) {
        for(int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[0].length; j++)
                System.out.print(array[i][j] + "\t");
            System.out.println();
        }
        System.out.println();
    }
}
