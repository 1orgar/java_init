public class Task10N042 {
    public static void main(String[] args) {
        double n = 12.1;
        int e = 5;
        System.out.println(n + "^" + e + "=" + exp(n, e));
    }
    static double exp(double n, int e) {
        if(e == 0) return 1;
        else return n * exp(n, e - 1);
    }
}
