import java.util.Scanner;

public class Task09N042 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter the word: ");
        StringBuilder sb =  new StringBuilder(sc.next());

        for(int i = 0; i < sb.length() / 2; i++) {
            char k = sb.charAt(i);
            sb.setCharAt(i, sb.charAt(sb.length() - i - 1));
            sb.setCharAt(sb.length() - i - 1, k);
        }
        System.out.println("String in reverse: " + sb);
    }
}