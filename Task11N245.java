public class Task11N245 {
    public static void main(String[] args) {
        int [] array = {10, 1, -17, 25, 66, -99, -87, 3, -20, 0, 34};
        int [] array2 = new int[array.length];

        for(int i = 0, is = 0, il = array.length - 1; i < array.length; i++) {
            if(array[i] < 0)
                array2[is++] = array[i];
            else
                array2[il--] = array[i];
        }

        for(int x:array2) {
            System.out.print(x + " ");
        }
    }
}
