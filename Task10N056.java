public class Task10N056 {
    public static void main(String[] args) {
        int num = 131071;
        System.out.print(num + " is" + (isPrimeNumber(num, 2) ? "" : " not") +" a prime number");
    }
    static boolean isPrimeNumber(int n, int div) {
        if(n <= 2)
            return true;
        else if(n % div == 0)
            return false;
        else if(n >= div * div)
            return isPrimeNumber(n, div + 1);
        else
            return true;
    }
}
