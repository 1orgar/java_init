import java.io.OutputStream;

public class Task04N033 {
    public static void main(String args[]) {
        int n = -101;
        n = (n ^ (n >> -1)) + (n >>> -1); // get abs, we rq natural number
        if(n % 2 == 0) {
            System.out.println("Число "+ n + " заканчивается четной цифрой");
        } else {
            System.out.println("Число "+ n + " заканчивается нечетной цифрой");
        }
    }
}
