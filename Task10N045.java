public class Task10N045 {
    public static void main(String[] args) {
        int first_member = 10, diff = 5, n = 10;
        System.out.println(n + " member of sequence = " + memb(n, first_member, diff));
        System.out.println("Sum of first " + n + " members of sequence = " + sum(n, first_member, diff));
    }
    static int memb(int n, int first, int diff) {
        if(n == 1) return first;
        else return diff + memb(n - 1, first, diff);
    }
    static int sum(int n, int first, int diff) {
        if(n == 1) return first;
        else return (diff * (n + 1)) + sum(n - 1, first, diff);
    }
}
