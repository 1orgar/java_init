import java.time.LocalDate;
import java.util.ArrayList;

public class Task13N012 {
    public static void main(String[] args) {
        ArrayList<Employee> ae = new ArrayList<>();
        ae.add(new Employee("Никифоров Чарльз Юхимович", "г. Суоярви, ул. Советская 2-я, дом 35, квартира 19", 2011, 1));
        ae.add(new Employee("Попов Павел Андреевич", "г. Богучаны, ул. Скобелевская, дом 35, квартира 356", 2017, 3));
        ae.add(new Employee("Васильев Платон Петрович", "c. Юрино, ул. Каменогорский 2-й пер, дом 81, квартира 15", 2014, 6));
        ae.add(new Employee("Андрусейко Альберт Викторович", "c. Суворово, ул. Горная  (Выборгский), дом 28, квартира 104", 2016, 8));
        ae.add(new Employee("Фомин Леопольд Фёдорович", "г. Верх-Усугли, ул. Клязьминская, дом 40, квартира 249", 2013, 5));
        ae.add(new Employee("Тарасов Леопольд Анатолиевич", "г. Сосногорск, ул. Ярославская, дом 13, квартира 315", 2010, 5));
        ae.add(new Employee("Шумило Владислав Владимирович", "г. Байконур, ул. Академика Понтрягина, дом 80, квартира 441", 2007, 9));
        ae.add(new Employee("Носков Никита Романович", "г. Верхнеуральск, ул. Парковая, дом 67, квартира 244", 2018, 4));
        ae.add(new Employee("Белозёров Чеслав Вадимович", "г. Таруса, ул. Глубокий пер, дом 93, квартира 46", 2012, 7));
        ae.add(new Employee("Бондаренко Шерлок Петрович", "г. Вохма, ул. Гришина, дом 73, квартира 386", 2011, 2));
        ae.add(new Employee("Белоусов Ян Леонидович", "с. Кантемировка, ул. Зональная, дом 39, квартира 255", 2010, 2));
        ae.add(new Employee("Гурьев Остап Валерьевич", "г. Назрань, ул. Кавалерийский пер, дом 10, квартира 114", 2009, 10));
        ae.add(new Employee("Полищук Ярослав Валерьевич", "с. Частые, ул. Ташкентская, дом 97, квартира 499", 2014, 1));
        ae.add(new Employee("Гончар Чарльз Леонидович", "п. Сосновское, ул. Бебеля, дом 86, квартира 219", 2018, 12));
        ae.add(new Employee("Яковенко Чарльз Александрович", "г. Клетский, ул. Взлетная  (Московский), дом 60, квартира 468", 2019, 11));
        ae.add(new Employee("Павленко Чеслав Дмитриевич", "п. Тюленино, ул. Комиссариатский пер, дом 92, квартира 33", 2010, 8));
        ae.add(new Employee("Бердник Денис Львович", "г. Курманаевка, ул. Природная, дом 3, квартира 101", 2011, 6));
        ae.add(new Employee("Комаров Яромир Брониславович", "г. Железногорск-Илимский, ул. Михайловский проезд, дом 50, квартира 39", 2012, 3));
        ae.add(new Employee("Михайлов Зураб Дмитриевич", "г. Бикин, ул. Гэсстроевский 8-й пер, дом 54, квартира 304", 2017, 5));

        LocalDate ld = LocalDate.now();
        int year = ld.getYear();
        int month = ld.getMonthValue();
        for(Employee e: ae) {
            if(year - (e.getYear() + (month == e.getMonth() ? 1 : 0)) <= 3)
                System.out.println(e.getInfo());
        }

    }
}

class Employee {
    private String first_name;
    private String last_name;
    private String middle_name;
    private String address;
    private int yaer;
    private int month;

    Employee(String fio, String address, int year, int month) {
        String [] na = fio.split(" ");
        this.first_name = na[0];
        this.last_name = na[1];
        this.middle_name = na[2];
        this.address = address;
        this.yaer = year;
        this.month = month;
    }

    public int getYear() {
        return yaer;
    }

    public int getMonth() {
        return month;
    }
    public String getInfo() {
        return first_name + " " + middle_name + " " + last_name + ", " + address;
    }
}