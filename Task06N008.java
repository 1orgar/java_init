public class Task06N008 {
    public static void main(String[] args) {
        int n = 1000;
        for(int i = 1; i * i <= n; i++) {
            System.out.println(i * i);
        }
    }
}
