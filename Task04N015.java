public class Task04N015 {
    public static void main(String args[]) {
        GatAge age = new GatAge(6, 1985);
        // test1
        age.set_current_date(12,2014);
        System.out.println(age.get_age());
        // test2
        age.set_current_date(5,2014);
        System.out.println(age.get_age());
        // test3
        age.set_current_date(6,2014);
        System.out.println(age.get_age());
    }
}

class GatAge {
    private int birthday_month, birthday_year;
    private int today_month, today_year;
    private int age;

    public GatAge(int birthday_month, int birthday_year) {
        this.birthday_month = birthday_month;
        this.birthday_year = birthday_year;
        today_month = 1;
        today_year = 1990;
        calc_age();
    }
    public void set_current_date(int today_month, int today_year) {
        this.today_month = today_month;
        this.today_year = today_year;
        calc_age();
    }
    private void calc_age() {
        age = today_year - birthday_year;
        if(today_month < birthday_month) age--;
    }
    public int get_age() {
        return age;
    }
}