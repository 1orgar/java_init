public class Task10N053 {
    public static void main(String[] args) {
        int [] arr = {15, 55, 171, 10, 98, 54, 117, 39, 0};
        printSequence(arr, 0);
    }
    static void printSequence(int [] m, int index) {
        if(m[index] == 0)
            return;;
        printSequence(m, index + 1);
        System.out.println(m[index]);
    }
}
