public class Task12N063 {
    public static void main(String[] args) {
        int [][] students = {
                {20, 15, 30, 11},
                {19, 31, 17, 12},
                {45, 11, 9, 16},
                {21, 25, 32, 12},
                {17, 24, 26, 19},
                {27, 20, 22, 29},
                {17, 16, 33, 10},
                {19, 15, 20, 17},
                {31, 20, 12, 25},
                {17, 13, 10, 7},
                {6, 9, 4, 3},
        };
        for(int i = 0; i < students.length; i++) {
            int avg = 0;
            for(int j: students[i])
                avg += j;
            avg /= students[i].length;
            System.out.println("Average number of students at " + (i + 1) + " parallel " + avg);
        }
    }
}
