public class Task03N029 {
    public static void main(String args[]) {
        int x = 5, y = 10, z = 50;
        /**
         * каждое из чисел X и Y нечетное;
         */
        if(x % 2 == 1 & y % 2 == 1)
            System.out.println("1 ok");
        /**
         * только одно из чисел X и Y меньше 20;
         */
        if(x < 20 ^ y < 20)
            System.out.println("2 ok");
        /**
         * хотя бы одно из чисел X и Y равно нулю;
         */
        if(x == 0 | y == 0)
            System.out.println("3 ok");
        /**
         * каждое из чисел X, Y, Z отрицательное;
         */
        if(x < 0 & y < 0 & z < 0)
            System.out.println("4 ok");
        /**
         * только одно из чисел X, Y и Z кратно пяти;
         */
        boolean xmod5 = x % 5 == 0, ymod5 = y % 5 == 0, zmod5 = z % 5 == 0;
        if((xmod5 ^ ymod5 ^ zmod5) & !(xmod5 & ymod5 & zmod5))
            System.out.println("5 ok");
        /**
         * хотя бы одно из чисел X, Y, Z больше 100.
         */
        if(x > 100 | y > 100 | z > 100)
            System.out.println("6 ok");
    }
}
