package TaskOOP;// Abstract class

public abstract class Candidate implements Human {
    //Protected
    protected String name;
    Candidate(String name) {
        this.name = name;
    }

    public void hello() {
        System.out.println("hi! my name is " + name + "!");
    }
    // Polymorphism
    public abstract void describeExperience();
    public abstract void describeExperience(String something);
}