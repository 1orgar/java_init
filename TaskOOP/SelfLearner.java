package TaskOOP;

public class SelfLearner extends Candidate {
    SelfLearner(String name) {
        super(name);
    }
    public void describeExperience() {
        describeExperience("");
    }
    public void describeExperience(String something) {
        System.out.println("I have been learning Java by myself, nobody examined how through is my knowledge and how good is my code. " + something);
    }
}