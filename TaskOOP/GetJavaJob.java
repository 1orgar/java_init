package TaskOOP;// Inheritance

public class GetJavaJob extends Candidate {
    GetJavaJob(String name) {
        // Super class call
        super(name);
    }
    public void describeExperience() {
        describeExperience("");
    }
    public void describeExperience(String something) {
        System.out.println("I passed successfully getJavaJob exams and code reviews. " + something);
    }
}