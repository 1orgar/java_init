// Package
package TaskOOP;

public class App {
    public static void main(String[] args) {
        Human empl = new Employee();
        Candidate [] cands = {new GetJavaJob("Mike"), new GetJavaJob("Kir"), new SelfLearner("Sam"),
        new SelfLearner("Tom"), new GetJavaJob("Elena"), new SelfLearner("Stan"),
        new SelfLearner("John"), new SelfLearner("Melissa"), new SelfLearner("Tim"), new GetJavaJob("Anna")};

        for(Candidate c: cands) {
            empl.hello();
            c.hello();
            c.describeExperience();
            System.out.println("");
        }
    }
}
