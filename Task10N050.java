public class Task10N050 {
    public static void main(String[] args) {
        int n = 4, m = 2; //  stack overflow
        System.out.println("Ackerman function result: " + fnAkk(n, m) + ", where n=" + n + ", m=" + m);
    }
    static int fnAkk(int n, int m) {
        if(n > 0 && m > 0)
            return fnAkk(n - 1, fnAkk(n, m - 1));
        else if(n != 0 && m ==0)
            return fnAkk(n - 1, 1);
        else
            return m + 1;
    }
}
