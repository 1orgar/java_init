import java.lang.Math;

public class Task12N024 {
    public static void main(String[] args) {
        int n = 6;
        int [][] array1 = new int[n][n];
        int [][] array2 = new int[n][n];

        System.out.println("A");
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                if(i == 0)
                    array1[i][j] = 1;
                else if(j == 0)
                    array1[i][j] = 1;
                else
                    array1[i][j] = array1[i - 1][j] + array1[i][j - 1];
                System.out.print(array1[i][j] + "\t");
            }
            System.out.println();
        }

        System.out.println("B");
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                array2[i][j] = (i + j) % n + 1;
                System.out.print(array2[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
